package be.rivendale.nutrition.configuration;

public enum  Profile {
    test("nutrition-test"),
    normal("nutrition");

    private String databaseName;

    private Profile(String databaseName) {
        this.databaseName = databaseName;
    }

    public String databaseName() {
        return databaseName;
    }
}
