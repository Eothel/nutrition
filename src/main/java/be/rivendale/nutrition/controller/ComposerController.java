package be.rivendale.nutrition.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ComposerController {
    @RequestMapping("composer")
    public String composer() {
        return "composer";
    }
}
