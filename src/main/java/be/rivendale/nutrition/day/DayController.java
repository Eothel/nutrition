package be.rivendale.nutrition.day;

import be.rivendale.nutrition.DateUtilities;
import be.rivendale.nutrition.domain.Ingredient;
import be.rivendale.nutrition.nutrient.Nutrient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@Transactional
@RequestMapping("day")
public class DayController {
    @PersistenceContext
    private EntityManager entityManager;

    @RequestMapping("{date}/add")
    public @ResponseBody Day add(@PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date, @RequestParam("nutrientId") int nutrientId, @RequestParam("weight") double weight) {
        Day day = retrieveExistingOrTransientDay(date);
        entityManager.persist(day);
        Nutrient nutrient = entityManager.find(Nutrient.class, nutrientId);
        day.addEntry(new Ingredient(nutrient, weight));
        return day;
    }

    @RequestMapping(method = POST, value = "{date}")
    public @ResponseBody Day dayJson(@PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
        return retrieveExistingOrTransientDay(date);
    }

    @RequestMapping(method = GET, value = "{date}")
    public String edit(@PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
        return "day/edit";
    }

    @RequestMapping(method = GET, value = "today")
    public String today() {
        return "redirect:" + DateUtilities.now().format(DateTimeFormatter.ISO_DATE);
    }

    private Day retrieveExistingOrTransientDay(Date date) {
        List<Day> results = entityManager.createQuery("select d from Day d where d.date = :date", Day.class)
                .setParameter("date", date)
                .getResultList();
        if(results.isEmpty()) {
            return new Day(date);
        } else {
            return results.get(0);
        }
    }
}
