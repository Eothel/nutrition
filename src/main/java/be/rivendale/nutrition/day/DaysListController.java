package be.rivendale.nutrition.day;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class DaysListController {
    @PersistenceContext
    private EntityManager entityManager;

    @RequestMapping(method = GET, value = "day/list")
    public void listPage() {
    }

    @RequestMapping(method = POST, value = "day/list")
    public @ResponseBody List<Day> list() {
        return entityManager.createQuery("select d from Day d order by d.date", Day.class).getResultList();
    }

}
