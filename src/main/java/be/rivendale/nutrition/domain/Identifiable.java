package be.rivendale.nutrition.domain;

public interface Identifiable {
    public Integer getId();
}
