package be.rivendale.nutrition.domain;

import be.rivendale.nutrition.nutrient.Nutrient;
import be.rivendale.nutrition.nutrient.NutrientComponent;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents a combination a nutrient and a weight, indicating a quantity of a specific nutrient.
 */
@Entity
public class Ingredient {
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Nutrient nutrient;

    private double weight;

    /**
     * Used by JPA.
     */
    protected Ingredient() {
    }

    public Ingredient(Nutrient nutrient, double weight) {
        checkNotNull(nutrient, "Ingredient must not have a null nutrient");
        this.nutrient = nutrient;
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public double quantity(NutrientComponent component) {
        return nutrient.getComponents().get(component) * weight;
    }

    public Map<NutrientComponent, Double> getComponents() {
        HashMap<NutrientComponent, Double> components = new HashMap<>();
        for (NutrientComponent component : NutrientComponent.values()) {
            components.put(component, quantity(component));
        }
        return components;
    }

    public Nutrient getNutrient() {
        return nutrient;
    }
}
