package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.domain.Ingredient;
import be.rivendale.nutrition.domain.NutrientException;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.*;

import static be.rivendale.nutrition.nutrient.NutrientComponent.energy;

@Entity
public class CompositeNutrient extends Nutrient {
    @OneToMany(cascade = CascadeType.PERSIST)
    private Set<Ingredient> ingredients = new HashSet<>();

    /**
     * Creates a Set of {@link Ingredient} with an equal weight from a Set of {@link SimpleNutrient}.
     * @param nutrients The input Set of nutrients that should all have equal weight.
     * @return A Set of {@link Ingredient}.
     */
    private static Ingredient[] toEqualIngredientsArray(Nutrient... nutrients) {
        if(nutrients == null) {
            return null;
        }

        Ingredient[] ingredients = new Ingredient[nutrients.length];
        for (int i = 0; i < nutrients.length; i++) {
            ingredients[i] = new Ingredient(nutrients[i], 1.0 / nutrients.length);
        }
        return ingredients;
    }

    private static List<Ingredient> normalize(Ingredient... ingredients) {
        checkIngredientsNotNullOrEmpty(ingredients);
        double totalWeight = totalWeight(ingredients);
        ArrayList<Ingredient> normalizedIngredients = new ArrayList<>();
        for (Ingredient ingredient : ingredients) {
            normalizedIngredients.add(new Ingredient(ingredient.getNutrient(), ingredient.getWeight() / totalWeight));
        }
        return normalizedIngredients;
    }

    private static void checkIngredientsNotNullOrEmpty(Ingredient... ingredients) {
        if(ingredients == null || ingredients.length == 0) {
            throw new NutrientException("A compound nutrient must have at least one member");
        }
    }

    private static double totalEnergy(List<Ingredient> ingredients) {
        double total = 0;
        for (Ingredient ingredient : ingredients) {
            total += ingredient.getComponents().get(energy);
        }
        return total;
    }

    private static double totalWeight(Ingredient[] ingredients) {
        double totalWeight = 0;
        for (Ingredient ingredient : ingredients) {
            totalWeight += ingredient.getWeight();
        }
        return totalWeight;
    }

    /**
     * Used by JPA.
     */
    protected CompositeNutrient() {
    }

    public CompositeNutrient(String name, String comments, Nutrient... nutrients) {
        this(name, comments, toEqualIngredientsArray(nutrients));
    }

    public CompositeNutrient(String name, String comments, Ingredient... ingredients) {
        super(name, null, totalEnergy(normalize(ingredients)), comments);
        this.ingredients.addAll(normalize(ingredients));
    }

    @Override
    public Map<NutrientComponent, Double> getComponents() {
        HashMap<NutrientComponent, Double> components = new HashMap<>();
        for (NutrientComponent component : NutrientComponent.values()) {
            components.put(component, component.sum(ingredients()));
        }
        return Collections.unmodifiableMap(components);
    }

    public Set<Ingredient> ingredients() {
        return Collections.unmodifiableSet(ingredients);
    }
}
