package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.domain.Ingredient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Controller
@Transactional
public class NutrientController {
    @PersistenceContext
    EntityManager entityManager;

    @RequestMapping(value = "/nutrients/get")
    public @ResponseBody List<Nutrient> get(@RequestParam(value = "filter", required = false, defaultValue = "") String nameFilter) {
        TypedQuery<Nutrient> query = entityManager.createQuery("select n from Nutrient n where lower(n.name) like lower(:name) order by n.name", Nutrient.class);
        query.setParameter("name", "%" + nameFilter + "%");
        return query.getResultList();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/nutrient/{nutrientId}")
    public @ResponseBody ResponseEntity<Nutrient> nutrientJson(@PathVariable("nutrientId") int nutrientId) {
        Nutrient nutrient = entityManager.find(Nutrient.class, nutrientId);
        LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("moreEnergyThanRatio", Double.toString(retrieveMoreEnergyThanRatio(nutrient)));
        return new ResponseEntity<>(nutrient, map, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/nutrient/{nutrientId}")
    public String nutrientHtml() {
        return "nutrient/details";
    }

    private double retrieveMoreEnergyThanRatio(Nutrient nutrient) {
        double numberOfNutrientsWithLessEnergy = entityManager.createQuery("select count(n) from Nutrient n where n.energy < :energy", Long.class)
                .setParameter("energy", nutrient.getEnergy())
                .getSingleResult();
        double totalNumberOfNutrients = entityManager.createQuery("select count(n) - 1 from Nutrient n", Long.class)
                .getSingleResult();
        return numberOfNutrientsWithLessEnergy / totalNumberOfNutrients;
    }

    @RequestMapping(method = RequestMethod.GET, value = "nutrient/create")
    public String createNutrientForm() {
        return "nutrient/edit";
    }

    @RequestMapping(method = RequestMethod.GET, value = "nutrient/{nutrientId}/edit")
    public String editNutrientForm(@PathVariable("nutrientId") int nutrientId) {
        return "nutrient/edit";
    }

    @RequestMapping(method = RequestMethod.GET, value = "nutrient/list")
    public void listNutrients() {
    }

    @RequestMapping(method = RequestMethod.POST, value = "nutrient/create")
    public @ResponseBody Nutrient createNutrient(@RequestBody CreateNutrientForm form) {
        Nutrient nutrient = asNutrient(form);
        entityManager.persist(nutrient);
        return nutrient;
    }

    @RequestMapping(method = RequestMethod.GET, value = "nutrient/{nutrientId}/ingredients")
    public @ResponseBody List<Ingredient> ingredientsByNutrient(@PathVariable("nutrientId") int nutrientId) {
        return entityManager.createQuery("select i from CompositeNutrient n join n.ingredients i join i.nutrient c where n.id = :nutrientId order by c.name", Ingredient.class)
                .setParameter("nutrientId", nutrientId)
                .getResultList();
    }

    private Nutrient asNutrient(CreateNutrientForm form) {
        if(form.type.equals("SimpleNutrient")) {
            return new SimpleNutrient(form.name,
                form.components.proteins / 100.0,
                form.components.carbs / 100.0,
                form.components.fats / 100.0,
                form.components.energy * 10.0,
                form.components.fruits / 100.0,
                form.components.vegetables / 100.0,
                form.comments, null
            );
        } else {
            ArrayList<Ingredient> ingredients = new ArrayList<>();
            for (CreateNutrientForm.IngredientForm ingredient : form.ingredients) {
                Nutrient nutrient = entityManager.getReference(Nutrient.class, ingredient.id);
                ingredients.add(new Ingredient(nutrient, ingredient.weight));
            }
            return new CompositeNutrient(form.name, form.comments, ingredients.toArray(new Ingredient[ingredients.size()]));
        }

    }
}
