package be.rivendale.nutrition.nutrient;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Entity
public class SimpleNutrient extends Nutrient {
    /**
     * Represents the macronutrient components of this nutrient per gram.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "components")
    @MapKeyEnumerated(EnumType.STRING)
    private Map<NutrientComponent, Double> components = new HashMap<>();

    /**
     * Used by JPA.
     */
    protected SimpleNutrient() {
    }

    public SimpleNutrient(String name, double proteins, double carbs, double fats, double energy, double fruits, double vegetables) {
        this(name, proteins, carbs, fats, energy, fruits, vegetables, null, null);
    }

    public SimpleNutrient(String name, double proteins, double carbs, double fats, double energy, double fruits, double vegetables, String comments, Serving serving) {
        super(name, serving, energy, comments);
        components.put(NutrientComponent.energy, energy);
        components.put(NutrientComponent.proteins, proteins);
        components.put(NutrientComponent.carbs, carbs);
        components.put(NutrientComponent.fats, fats);
        components.put(NutrientComponent.fruits, fruits);
        components.put(NutrientComponent.vegetables, vegetables);
    }

    @Override
    public boolean equals(Object object) {
        SimpleNutrient nutrient = (SimpleNutrient)object;
        return this.getName().equals(nutrient.getName()) && components.equals(nutrient.components);
    }

    @Override
    public int hashCode() {
        return getName().hashCode() * components.hashCode();
    }

    @Override
    public Map<NutrientComponent, Double> getComponents() {
        return Collections.unmodifiableMap(components);
    }
}
