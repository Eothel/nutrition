var nutrition = angular.module("nutrition", []);

nutrition.controller('DayController', ['$scope', '$http', function($scope, $http) {
    $scope.$watch('nutrientFilter', function(value) {
        if($scope.pendingNutrientFilterHandle !== undefined) {
            clearTimeout($scope.pendingNutrientFilterHandle);
            $scope.pendingNutrientFilterHandle = undefined;
        }

        if(value === undefined || value === '') {
            $scope.filteredNutrients = [];
        } else {
            $scope.pendingNutrientFilterHandle = setTimeout(function() {
                $http({method: 'get', url: '/api/nutrient/all?filter=' + value}).success(function (nutrients) {
                    $scope.filteredNutrients = nutrients;
                });
            }, 200);
        }
    });

    $scope.selectNutrient = function(index) {
        $scope.newEntry = {
            nutrient: $scope.filteredNutrients[index]
        };
        $scope.filteredNutrients = [];
    };

    $scope.cancelEntry = function() {
        $scope.newEntry = undefined;
        $scope.nutrientFilter = undefined;
    };

    $scope.confirmEntry = function() {
        $scope.newEntry.nutrient = $scope.newEntry.nutrient.id;
        $http.put('/api/day/' + lastPathSegment() + '/entry', $scope.newEntry).success(function(day) {
            assignDay(day);
            $scope.cancelEntry();
        });
    };

    $scope.removeEntry = function(index, $event) {
        index = $scope.day.entries.length - index - 1;
        var entry = $scope.day.entries[index];
        $http.delete('/api/day/' + lastPathSegment() + '/entry/' + index).success(function(day) {
            assignDay(day);
        });
        $event.stopPropagation();
    };

    $scope.toggleDetails = function(index) {
        var entry = $scope.day.entries[index];
        entry.details = entry.details === undefined ? true : undefined;
    };

    function assignDay(day) {
        if(day === '') {
            $scope.day = {date: new Date(dateString), entries: []};
        } else {
            day.entries.reverse();
            $scope.day = day;
        }
    }

    var dateString = lastPathSegment();
    $http({method: 'get', url: '/api/day/' + dateString}).success(function(day) {
        assignDay(day);
    });
}]);

nutrition.controller('DayListController', ['$scope', '$http', function($scope, $http) {
    $scope.today = function() {
        return new Date();
    };

    $http({method: 'get', url: '/api/day/all'}).success(function(days) {
        $scope.days = days;
    });
}]);