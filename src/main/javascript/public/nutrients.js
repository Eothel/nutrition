var nutrition = angular.module("nutrition", []);

nutrition.controller('NutrientController', ['$scope', '$http', function($scope, $http) {
    $http({method: 'get', url: '/api/nutrient/' + lastPathSegment()}).success(function(nutrient) {
        $scope.nutrient = nutrient;
    });
}]);

nutrition.controller('NutrientListController', ['$scope', '$http', function($scope, $http) {
    $http({method: 'get', url: '/api/nutrient/all'}).success(function(nutrients) {
        $scope.nutrients = nutrients;
    });

    $scope.accuracy = function(nutrient) {
        // TODO: Also add (8 * fiber)
        var calculatedEnergy = (17000 * (nutrient.proteins + nutrient.carbs)) + (37000 * nutrient.fats);
        return  100 * (calculatedEnergy / nutrient.energy);
    };
}]);