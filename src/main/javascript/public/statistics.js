var nutrition = angular.module("nutrition", []);

nutrition.controller('StatisticsController', ['$scope', '$http', function($scope, $http) {
    $scope.updateStatistics = function() {
        console.log('Updating statistics from %s to %s', $scope.from, $scope.to);
        var components = ['energy', 'proteins', 'carbs', 'fats', 'fruits', 'vegetables'];
        $scope.statistics = {
            selection: {
                energy: {active: true, map: function(value) { return value / 1000; }},
                proteins: {active: true},
                carbs: {active: true},
                fats: {active: true},
                fruits: {active: true},
                vegetables: {active: true}
            },
            averages: {
                ratio: function(component) {
                    return this[component] / (this.proteins + this.fats + this.carbs);
                }
            },
            days: $scope.days.filter(function(day) {
                var date = new Date(Date.parse(day.date));
                return isDateInRange(date, $scope.from, $scope.to);
            })
        };
        components.forEach(function(component) {
            var total = $scope.statistics.days.reduce(function(previous, day) {
                return previous + day[component];
            }, 0);
            $scope.statistics.averages[component] = total / $scope.statistics.days.length;
        });
        $scope.drawChart();
    };

    $scope.drawChart = function() {
        var header = ['date'];
        for(var component in $scope.statistics.selection) {
            if($scope.statistics.selection[component].active === true) {
                header.push(component);
            }
        }

        var data = $scope.statistics.days.map(function(day) {
            var row = [new Date(Date.parse(day.date))];
            for(var component in $scope.statistics.selection) {
                addIfSelected(row, day, component);
            }
            return row;
        }).reverse();

        data.unshift(header);
        var chart = new google.visualization.LineChart(document.getElementById('chart'));
        chart.draw(google.visualization.arrayToDataTable(data), {
            vAxis: {
                minValue: 0
            }
        });
    };

    function addIfSelected(row, day, component) {
        var selection = $scope.statistics.selection[component];
        if(selection.active === true) {
            if(selection.map !== undefined) {
                row.push(selection.map(day[component]));
            } else {
                row.push(day[component]);
            }
        }
    }

    function isDateInRange(date, from, to) {
        return date >= from && date <= to;
    }

    function newOffsettedDate(dayOffsetFromToday) {
        var date = new Date();
        date.setDate(date.getDate() + dayOffsetFromToday);
        return date;
    }

    $http({method: 'get', url: '/api/day/all'}).success(function(days) {
        $scope.days = days;
        $scope.from = newOffsettedDate(-31);
        $scope.to = newOffsettedDate(-1);
        $scope.updateStatistics();
    });
}]);