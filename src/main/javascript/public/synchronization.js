var nutrition = angular.module('nutrition', []);

nutrition.controller('SynchronizationController', ['$scope', '$http', function($scope, $http) {
    $scope.synchronize = function() {
        $scope.status = 'SYNCHRONIZING';
        $http({method: 'get', url: '/api/synchronize'}).success(function() {
            $scope.status = 'SUCCESS';
        }).error(function() {
            $scope.status = 'ERROR';
        });
    };

    $scope.status = '';
}]);