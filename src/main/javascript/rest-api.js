var async = require('async');

var express = require('express');
var router = express.Router();

var schema = require('./schema/schema');
var synchronizer = require('./synchronizer/synchronize');

router.get('/synchronize', function(request, response) {
    synchronizer.synchronize(graceReply(response, false, 204));
});

router.get('/day/all', function(request, response) {
    schema.Day.find().sort('-date').exec(graceFilter(response, true, function(days) {
        async.each(days, function(day, callback) {
            day.recursivePopulate(callback);
        }, graceReply(response, true, days));
    }));
});

router.get('/day/:date', function(request, response) {
    var date = parseLocalDate(request.param('date'));
    schema.Day.findByDate(date, true, graceReply(response, false, function(day) {
        return day === null ? 204 : day;
    }));
});

router.delete('/day/:date/entry/:index', function(request, response) {
    var date = parseLocalDate(request.param('date'));
    var index = request.param('index');
    console.log('Removing entry with index `%d` from day with date `%s`', index, date.toISOString());

    schema.Day.findByDate(date, false, graceFilter(response, true, function(day) {
        day.removeEntry(index);
        if(day.entries.length == 0) {
            console.log('Removing day with date `%s` because there are no more entries.', date.toISOString());
            day.remove(graceReply(response, false, 204));
        } else {
            day.saveAndPopulate(graceReply(response, true));
        }
    }));
});

router.put('/day/:date/entry', function(request, response) {
    var date = parseLocalDate(request.param('date'));
    var entry = request.body;
    console.log('Adding entry to day `' + date + '` with value `' + JSON.stringify(entry) + '`');

    schema.Day.findByDate(date, false, graceFilter(response, false, function(day) {
        if(day === null) {
            day = new schema.Day({date: date});
        }
        day.addEntry(entry);
        day.saveAndPopulate(graceReply(response, true));
    }));
});

router.get('/nutrient/all', function(request, response) {
    var filter = request.param('filter') === undefined ? '' : request.param('filter');
    schema.Nutrient.find({name: new RegExp('^.*' + filter + '.*$', 'i')}).sort('name').exec(graceFilter(response, true, function(nutrients) {
        async.each(nutrients, function(nutrient, callback) {
            nutrient.recursivePopulate(callback);
        }, graceReply(response, true, nutrients));
    }));
});

router.get('/nutrient/:id', function(request, response) {
    schema.Nutrient.findById(request.param('id'), graceFilter(response, true, function(nutrient) {
        nutrient.recursivePopulate(graceReply(response, true, nutrient));
    }));
});

/**
 * Provides a graceful reply in response to an asynchronous call.
 * @param response The HTTP response to send the reply through.
 * @param mandatory When true, a non-null result is expected. If not, returns HTTP status code 404 (Not Found)
 * @param [send] what to send as the response when successful. Can be either a value or a function(value) (in which case the
 * result of invoking that function will be returned).
 * @return A typical asynchronous callback function(error, result) bound to provide a graceful reply in case of errors or no results.
 */
function graceReply(response, mandatory, send) {
    return graceFilter(response, mandatory, function(result) {
        if(typeof(send) === 'function') {
            response.send(send(result));
        } else if(send !== undefined) {
            response.send(send);
        } else {
            response.send(result);
        }
    });
}

/**
 * Provides a graceful handler that filters out any possible error conditions.
 * @param response The response to send possible error messages through.
 * @param mandatory True when there must be a response to send. If not, a 404 Not Found is sent.
 * @param callback A callback that will be called, after all error conditions have been filtered out.
 * @return {Function} A typical asynchronous callback(error, result) function bound to provide a graceful reply in case of errors ir no results.
 */
function graceFilter(response, mandatory, callback) {
    return function(error, result) {
        if(error !== null && error !== undefined) {
            response.send(error.message, 500);
        } else if(result === null && mandatory === true) {
            response.send(404);
        } else {
            callback(result);
        }
    };
}

/**
 * Parses in input date with an ISO pattern (yyyy-MM-dd) as a date in the current locale.
 * @param isoDate The date string to parse.
 * @return {Date} The date parsed as a local timezone date.
 */
function parseLocalDate(isoDate) {
    var parts = /^(\d{4})-(\d{2})-(\d{2})$/.exec(isoDate);
    if(parts === null || parts.length !== 1 + 3) {
        throw new Error('Invalid date pattern. Expected `yyyy-MM-dd` but was `' + isoDate + '`');
    }
    return new Date(parseInt(parts[1]), parseInt(parts[2]) - 1, parseInt(parts[3]));
}

exports.router = router;