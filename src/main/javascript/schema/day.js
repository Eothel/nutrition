var async = require('async');

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var shared = require('./shared');
var ingredient = require('./ingredient');

var daySchema = new Schema({
    date: {type: Date, required: true},
    entries: [ingredient.ingredientSchema]
}, {id: false});

daySchema.path('entries').validate(shared.arrayNotEmpty, 'At least one entry is required');

shared.addVirtualIngredientReducingComponent(daySchema, 'energy', 'entries');
shared.addVirtualIngredientReducingComponent(daySchema, 'proteins', 'entries');
shared.addVirtualIngredientReducingComponent(daySchema, 'carbs', 'entries');
shared.addVirtualIngredientReducingComponent(daySchema, 'fats', 'entries');
shared.addVirtualIngredientReducingComponent(daySchema, 'fruits', 'entries');
shared.addVirtualIngredientReducingComponent(daySchema, 'vegetables', 'entries');

daySchema.set('toJSON', {virtuals: true});

daySchema.methods.recursivePopulate = function(callback) {
    console.log('Populating day with date %s', this.date);
    exports.Day.populate(this, {path: 'entries.nutrient'}, function(error, day) {
        async.each(day.entries, function(entry, callback) {
            entry.nutrient.recursivePopulate(callback);
        }, callback);
    });
};

daySchema.methods.addEntry = function(entry) {
    this.entries.push(entry);
};

daySchema.methods.removeEntry = function(index) {
    this.entries.splice(index, 1);
};

daySchema.methods.saveAndPopulate = function(callback) {
    console.log('Saving day with date %s', this.date);
    this.save(function(error, day) {
        if(error) {
            callback(error, undefined);
        } else {
            day.recursivePopulate(function(error) {
                if(error) {
                    callback(error, undefined);
                } else {
                    callback(null, day);
                }
            });
        }
    })
};

daySchema.statics.findByDate = function(date, populate, callback) {
    console.log('Find day by date `%s`', date.toISOString());
    exports.Day.findOne({date: date}, function(error, day) {
        if(error) {
            callback(error, undefined);
        } else if(day === null) {
            callback(null, null);
        } else if(populate) {
            day.recursivePopulate(function(error) {
                if(error) {
                    callback(error, undefined);
                } else {
                    callback(null, day);
                }
            });
        } else {
            callback(null, day);
        }
    });
};

exports.Day = mongoose.model('day', daySchema);