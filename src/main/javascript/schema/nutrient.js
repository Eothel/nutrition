var util = require('util');
var async = require('async');

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var shared = require('./shared');
var ingredient = require('./ingredient');

function BaseNutrientSchema() {
    Schema.apply(this, arguments);
}
util.inherits(BaseNutrientSchema, Schema);

var nutrientSchema = new BaseNutrientSchema({
    name: {type: String, required: true},
    comments: String,
    servings: [{
        label: {type: String, required: true},
        measurements: {type: Array, validate: [
            {validator: shared.arrayNotEmpty, msg: 'At least one entry is required for "{PATH}".'},
            {validator: arrayOfNumbers, msg: 'Entries must be numbers for "{PATH}".'}
        ]}
    }]
});

nutrientSchema.set('toJSON', {virtuals: true});

var Nutrient = mongoose.model('nutrients', nutrientSchema);

var simpleNutrientSchema = new BaseNutrientSchema({
    energy: {type: Number, required: true, min: 0, max: 37000},
    proteins: {type: Number, required: true, min: 0, max: 1},
    carbs: {type: Number, required: true, min: 0, max: 1},
    fats: {type: Number, required: true, min: 0, max: 1},
    fruits: {type: Number, required: true, min: 0, max: 1},
    vegetables: {type: Number, required: true, min: 0, max: 1}
});

simpleNutrientSchema.methods.recursivePopulate = function(callback) {
    callback(null, this);
};

var SimpleNutrient = Nutrient.discriminator('simple', simpleNutrientSchema);

var compositeNutrientSchema = new BaseNutrientSchema({
    ingredients: [ingredient.ingredientSchema]
});

compositeNutrientSchema.path('ingredients').validate(shared.arrayNotEmpty, 'At least one ingredient is required');

shared.addVirtualIngredientReducingComponent(compositeNutrientSchema, 'energy', 'ingredients');
shared.addVirtualIngredientReducingComponent(compositeNutrientSchema, 'fruits', 'ingredients');
shared.addVirtualIngredientReducingComponent(compositeNutrientSchema, 'proteins', 'ingredients');
shared.addVirtualIngredientReducingComponent(compositeNutrientSchema, 'carbs', 'ingredients');
shared.addVirtualIngredientReducingComponent(compositeNutrientSchema, 'fats', 'ingredients');
shared.addVirtualIngredientReducingComponent(compositeNutrientSchema, 'vegetables', 'ingredients');

compositeNutrientSchema.methods.recursivePopulate = function(callback) {
    Nutrient.populate(this, {path: 'ingredients.nutrient'}, function(error, nutrient) {
        async.each(nutrient.ingredients, function(ingredient, callback) {
            ingredient.nutrient.recursivePopulate(callback);
        }, callback);
    });
};

var CompositeNutrient = Nutrient.discriminator('composite', compositeNutrientSchema);

function arrayOfNumbers(array) {
    for(var i = 0; i < array.length; i++) {
        if(typeof(array[i]) !== 'number') {
            return false;
        }
    }
    return true;
}

exports.Nutrient = Nutrient;
exports.CompositeNutrient = CompositeNutrient;
exports.SimpleNutrient = SimpleNutrient;