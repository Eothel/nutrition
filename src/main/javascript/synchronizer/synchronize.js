var mongoose = require('mongoose');
var async = require('async');
var assert = require('assert');

var mysql = require('./mysql');
var schema = require('./../schema/schema');
var configuration = require('./../configuration');

var ObjectId = mongoose.Types.ObjectId;

module.exports.synchronize = function(callback) {
    console.log('Starting migration of Nutrition database from MySQL (%s) to MongoDB (%s/%s)',
        JSON.stringify(configuration.mysql),
        mongoose.connection.host, mongoose.connection.name);
    console.log('Migrating Nutrients');
    mysql.loadNutrients(configuration.mysql, function(nutrients) {
        nutrients.forEach(addObjectId);
        nutrients.forEach(resolveIngredientReferences.bind(null, nutrients));

        console.log('Saving nutrients in MongoDB: ' + nutrients.length);
        clearCollections(function(error) {
            if(error) {
                callback(error);
            } else {
                async.eachSeries(nutrients.map(nutrientToMongoose), persistNutrient, migrateDays.bind(null, nutrients, callback));
            }
        });
    });
};

function migrateDays(nutrients, callback) {
    console.log('Migrating Days');
    mysql.loadDays(configuration.mysql, function(days) {
        console.log('Saving days in MongoDB: ' + days.length);
        days.forEach(resolveEntryReferences.bind(null, nutrients, days));

        async.eachSeries(days.map(dayToMongoose), persistDay, function() {
            callback();
        });
    });
}

function resolveEntryReferences(nutrients, days) {
    days.forEach(function(day) {
        day.entries.forEach(function(entry) {
            entry.nutrient = find(entry.migrate.nutrientId);
        });
    });

    function find(oldNutrientId) {
        for(var i = 0; i < nutrients.length; i++) {
            var nutrient = nutrients[i];
            if(nutrient.migrate.id === oldNutrientId) {
                return nutrient._id;
            }
        }
        throw new Error('Unable to resolve day entry for nutrient id `' + oldNutrientId + '`');
    }
}

/**
 * Adds an ObjectId reference to each nutrient.
 */
function addObjectId(nutrient) {
    nutrient._id = ObjectId();
}

/**
 * Translates MySQL foreign key ID columns to Mongoose ObjectId references.
 */
function resolveIngredientReferences(nutrients, nutrient) {
    if(nutrient.ingredients) {
        nutrient.ingredients.forEach(function(ingredient) {
            var migrateIngredientNutrientId = ingredient.migrate.nutrientId;
            ingredient.nutrient = find(nutrients, migrateIngredientNutrientId);
        });
    }
    return nutrient;

    function find(nutrients, oldIngredientId) {
        for(var i = 0; i < nutrients.length; i++) {
            var nutrient = nutrients[i];
            if(nutrient.migrate.id === oldIngredientId) {
                return nutrient._id;
            }
        }
        throw new Error('Unable to resolve nutrient id for ingredient of nutrient `' + nutrient.name + '`');
    }
}

/**
 * Maps an input raw object to a Mongoose Model.
 * @param nutrient The raw nutrient object to convert.
 * @return A Mongoose Nutrient Model (SimpleNutrient or CompositeNutrient).
 */
function nutrientToMongoose(nutrient) {
    switch(nutrient.migrate.type) {
        case 'simple': return new schema.SimpleNutrient(nutrient); break;
        case 'composite': return new schema.CompositeNutrient(nutrient); break;
        default: throw new Error('Unrecognized nutrient type `' + nutrient.migrate.type + '`');
    }
}

/**
 * Maps an input raw day object to a Mongoose Model.
 * @param day The ray day to convert.
 * @return A Mongoose Day Model.
 */
function dayToMongoose(day) {
    return new schema.Day(day);
}

/**
 * Stores a Mongoose model in MongoDB.
 * @param nutrient The nutrient to save.
 * @param callback Called upon completion of the async operations.
 */
function persistNutrient(nutrient, callback) {
    nutrient.save(function(error) {
        if(error) throw new Error('Error while trying to save nutrient `' + nutrient.name + '` to to MongoDB: ' + error);
        console.log('\t- Saved nutrient `' + nutrient.name + '`');
        callback();
    });
}

/**
 * Saves a Mongoose Day model in MongoDB.
 * @param day The day object to save.
 * @param callback Called upon completion of the async operations.
 */
function persistDay(day, callback) {
    day.save(function(error) {
        if(error) throw new Error('Error while trying to save day `' + day.date + '` to to MongoDB:' + error);
        console.log('\t- Saved day `' + day.date.toISOString() + '`');
        callback();
    });
}

/**
 * Clears the `nutrients` and `days` collections.
 * @param callback invoked as 'callback(error)'.
 */
function clearCollections(callback) {
    schema.Nutrient.remove({}, function(error) {
        if(error) {
            callback(error);
        } else {
            schema.Day.remove({}, function (error) {
                callback(error);
            });
        }
    });
}