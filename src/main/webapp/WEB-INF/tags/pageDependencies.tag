<%@ taglib prefix="food" tagdir="/WEB-INF/tags" %>

<food:style cssResource="bootstrap.css"/>
<food:style cssResource="bootstrap-theme.css"/>

<food:script jsResource="angular-1.2.6.js"/>
<food:script jsResource="ui-bootstrap-custom-0.9.0.js"/>
<food:script jsResource="ui-bootstrap-custom-tpls-0.9.0.js"/>