<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="jsResource" required="true" type="java.lang.String" %>

<spring:eval expression="T(be.rivendale.nutrition.configuration.WebConfiguration).EXPOSED_RESOURCE_PATH" var="resourcesPathPrefix"/>
<c:url var="scriptUrl" value="${resourcesPathPrefix}/${jsResource}"/>
<script type="application/javascript" src="${scriptUrl}"></script>