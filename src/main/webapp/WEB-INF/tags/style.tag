<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="cssResource" required="true" type="java.lang.String" %>

<spring:eval expression="T(be.rivendale.nutrition.configuration.WebConfiguration).EXPOSED_RESOURCE_PATH" var="resourcesPathPrefix"/>
<c:url var="cssStyleUrl" value="${resourcesPathPrefix}/${cssResource}"/>
<link type="text/css" rel="stylesheet" href="${cssStyleUrl}"/>