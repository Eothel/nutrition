<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<script type="application/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.js"></script>

<html>
    <head>
        <title>Days</title>

        <meta name="viewport" content="width=device-width"/>

        <link rel="stylesheet" type="text/css" href="/resources/style.css"/>

        <script type="application/javascript">
            var nutrition = angular.module("nutrition", []);

            nutrition.controller('DayListController', ['$scope', '$http', function($scope, $http) {
                $http.post('/day/list').success(function(data) {
                    $scope.days = data;
                });

                $scope.openSelectedDay = function(day) {
                    window.open('/day/' + day, '_self');
                }
            }]);
        </script>
    </head>
    <body ng-app="nutrition" ng-controller="DayListController">
        <header>
            <h1>List of registered days</h1>
            <p>This page shows a list of all days with nutrient registrations.</p>
        </header>
        <main>
            <section class="card large">
                <header>Days</header>
                <table>
                    <tr>
                        <th>Date</th>
                        <th>Energy</th>
                        <th>Proteins</th>
                        <th>Carbs</th>
                        <th>Fats</th>
                        <th>Fruits</th>
                        <th>Vegetables</th>
                    </tr>
                    <tr ng-repeat="day in days" class="selectable" ng-click="openSelectedDay(day.date)">
                        <td>{{day.date}}</td>
                        <td class="numeric">{{day.current.energy / 1000 | number: 0}}kJ</td>
                        <td class="numeric">{{day.current.proteins | number: 2}}g</td>
                        <td class="numeric">{{day.current.carbs | number: 2}}g</td>
                        <td class="numeric">{{day.current.fats | number: 2}}g</td>
                        <td class="numeric">{{day.current.fruits | number: 2}}g</td>
                        <td class="numeric">{{day.current.vegetables | number: 2}}g</td>
                    </tr>
                </table>
                <div>
                    <a href="/day/today">Today</a>
                </div>
            </section>
        </main>
    </body>
</html>
