<%@ taglib prefix="food" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>

<html ng-app>
    <head>
        <title>Meal</title>
        <food:pageDependencies/>
        <script type="application/javascript">
            function MealController($scope, $http) {
                $http.get('<c:url value="/nutrients/get"/>').success(function(data) {
                    $scope.nutrients = data;
                });
                clearBreakdown();

                $scope.removeIngredient = function(index) {
                    $scope.ingredients.splice(index, 1);
                    $scope.breakdown();
                };

                $scope.addIngredient = function() {
                    $scope.ingredients.push({
                        nutrient: $scope.nutrient,
                        weight: $scope.nutrient.serving != null ? $scope.nutrient.serving.weight : 1
                    });
                    $scope.breakdown();
                };

                function clearBreakdown() {
                    $scope.macronutrientBreakdown = {proteins: 0, carbs: 0, fats: 0, energy: 0};
                }

                $scope.breakdown = function() {
                    clearBreakdown();
                    angular.forEach($scope.ingredients, function(ingredient) {
                        $scope.macronutrientBreakdown.proteins = $scope.round($scope.macronutrientBreakdown.proteins + (ingredient.nutrient.proteins * ingredient.weight), 1);
                        $scope.macronutrientBreakdown.carbs = $scope.round($scope.macronutrientBreakdown.carbs + (ingredient.nutrient.carbs * ingredient.weight), 1);
                        $scope.macronutrientBreakdown.fats = $scope.round($scope.macronutrientBreakdown.fats + (ingredient.nutrient.fats * ingredient.weight), 1);
                        $scope.macronutrientBreakdown.energy = $scope.round(($scope.macronutrientBreakdown.energy + (ingredient.nutrient.energy * ingredient.weight) / 1000), 0);
                    });
                };

                $scope.round = function(value, decimalPlaces) {
                    var factor = Math.pow(10, (decimalPlaces));
                    return(Math.round(value * factor) / factor);
                };

                $scope.ingredients = [];

                $scope.goal = "stay";
                $scope.bodyWeight = 73;
                $scope.bodyFayPercentage = 17;
                $scope.activityMultiplier= 1.5;
                $scope.mealsPerDay= 4;

                $scope.help = function() {
                    alert('Ask kevin what this means.');
                };

                $scope.calculateBmr = function() {
                    return 370 + (21.6 * (1 - ($scope.bodyFayPercentage / 100)) * $scope.bodyWeight);
                };

                $scope.calculateDailyCalories = function() {
                    return $scope.calculateBmr() * $scope.activityMultiplier;
                };

                $scope.calculateGoalCalories = function() {
                    var multiplier = 1;
                    if($scope.goal == 'loose') {
                        multiplier -= 0.15;
                    } else if($scope.goal == 'gain') {
                        multiplier += 0.15;
                    }
                    return $scope.calculateDailyCalories() * multiplier;
                };

                $scope.calculateGoalProteins = function() {
                    return $scope.bodyWeight * 2.5;
                };

                $scope.calculateGoalCarbs = function() {
                    return ($scope.calculateGoalCalories() - ($scope.calculateGoalProteins() * 4 + $scope.calculateGoalFats() * 9)) / 4;
                };

                $scope.calculateGoalFats = function() {
                    return $scope.bodyWeight * 1.5;
                };
            }
        </script>
    </head>
    <body ng-controller="MealController">
        <div class="container">
            <div class="jumbotron">
                <h1>Meal</h1>
                <p>This page allows you to compose a meal, which is super cool!</p>
            </div>

            <div class="page-header"><h2>Daily intake</h2></div>
            <div class="form-group">
                <label for="bodyWeight">Weight (kg)</label>
                <input id="bodyWeight" class="form-control" type="number" ng-model="bodyWeight"/>
            </div>
            <div class="form-group">
                <label for="bodyFatPercentage">Body fat (%)</label>
                <input id="bodyFatPercentage" class="form-control" type="number" ng-model="bodyFayPercentage"/>
            </div>
            <div class="form-group">
                <label for="bodyFatPercentage">Activity multiplier <small><a href="" ng-click="help()">?</a></small></label>
                <input id="activityMultiplier" class="form-control" type="number" ng-model="activityMultiplier"/>
            </div>
            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="radio" ng-model="goal" value="gain"/> Gain weight
                </label>
                <label class="checkbox-inline">
                    <input type="radio" ng-model="goal" value="loose"/> Loose weight
                </label>
                <label class="checkbox-inline">
                    <input type="radio" ng-model="goal" value="stay"/> Stay the same
                </label>
            </div>
            <div class="alert alert-info">
                <p>You have <span class="badge">{{round(bodyWeight * (bodyFayPercentage / 100), 1)}}kg</span> fat and <span class="badge">{{round(bodyWeight * (1 - (bodyFayPercentage / 100)), 1)}}kg</span> lean body mass.</p>
                <p>Your BMR (base metabolic rate) is <span class="badge">{{round(calculateBmr(), 0)}}kcal</span>.</p>
                <p>Your maintenance calorie level is <span class="badge">{{round(calculateDailyCalories(), 0)}}kcal</span>.</p>
                <p>Your daily intake should be:</p>
                <table>
                    <tr>
                        <th>Proteins</th>
                        <td>{{round(calculateGoalProteins(), 0)}}g</td>
                    </tr>
                    <tr>
                        <th>Carbs</th>
                        <td>{{round(calculateGoalCarbs(), 0)}}g</td>
                    </tr>
                    <tr>
                        <th>Fats</th>
                        <td>{{round(calculateGoalFats(), 0)}}g</td>
                    </tr>
                    <tr>
                        <th>Energy</th>
                        <td>{{round(calculateGoalCalories(), 0)}}kcal</td>
                    </tr>
                </table>
            </div>

            <div>
                <div class="page-header"><h2>Meal composer</h2></div>
                <h3>Choose nutrient</h3>
                <div class="form-group">
                    <label for="nutrient">Nutrient</label>
                    <select id="nutrient" class="form-control" ng-model="nutrient" ng-options="nutrient.name for nutrient in nutrients"></select>
                </div>
                <div>
                    <ul class="list-group" style="margin-bottom: 0;">
                        <li class="list-group-item">Proteins*: <span ng-if="nutrient != null">{{round(nutrient.proteins * 100, 1)}}g</span></li>
                        <li class="list-group-item">Carbs*: <span ng-if="nutrient != null">{{round(nutrient.carbs * 100, 1)}}g</li>
                        <li class="list-group-item">Fats*: <span ng-if="nutrient != null">{{round(nutrient.fats * 100, 1)}}g</li>
                        <li class="list-group-item">Energy*: <span ng-if="nutrient != null">{{round(nutrient.energy * 100 / 1000, 0)}}kJ</li>
                        <li class="list-group-item">
                            <span ng-if="nutrient != null && nutrient.serving != null">Servings of "{{nutrient.serving.label}}": {{round(nutrient.serving.weight, 0)}}g</span>
                            <span ng-if="nutrient == null || nutrient.serving == null">No typical serving size</span>
                        </li>
                    </ul>
                    <small>* Nutrient breakdown per 100g</small>
                </div>
                <button class="btn btn-sm btn-primary" ng-click="addIngredient()">Add</button>

                <h3>Meal ingredients</h3>
                <table class="table">
                    <tr ng-repeat="ingredient in ingredients">
                        <td>{{ingredient.nutrient.name}}</td>
                        <td><input type="text" ng-change="breakdown()" ng-model="ingredient.weight"/>
                            <small>g</small>
                        </td>
                        <td>
                            <button class="btn btn-xs" ng-click="removeIngredient($index)">Remove</button>
                        </td>
                    </tr>
                </table>
                <hr/>
            </div>

            <div>
                <div class="form-group">
                    <label for="mealsPerDay">Meals per day</label>
                    <input id="mealsPerDay" class="form-control" type="number" ng-model="mealsPerDay"/>
                </div>
                <table class="table">
                    <tr>
                        <th></th>
                        <th>Actual</th>
                        <th>Goal</th>
                    </tr>
                    <tr>
                        <th>Proteins</th>
                        <td>{{macronutrientBreakdown.proteins}}g</td>
                        <td>{{round(calculateGoalProteins() / mealsPerDay, 0)}}g</td>
                    </tr>
                    <tr>
                        <th>Carbs</th>
                        <td>{{macronutrientBreakdown.carbs}}g</td>
                        <td>{{round(calculateGoalCarbs() / mealsPerDay, 0)}}g</td>
                    </tr>
                    <tr>
                        <th>Fats</th>
                        <td>{{macronutrientBreakdown.fats}}g</td>
                        <td>{{round(calculateGoalFats() / mealsPerDay, 0)}}g</td>
                    </tr>
                    <tr>
                        <th>Energy</th>
                        <td>{{macronutrientBreakdown.energy}}kJ ({{round(macronutrientBreakdown.energy * 0.239005736, 0)}}kcal)</td>
                        <td>{{round(calculateGoalCalories() / mealsPerDay, 0)}}kcal</td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
