<html>
    <head>
        <title>Create nutrient</title>

        <link rel="stylesheet" type="text/css" href="/resources/style.css"/>

        <style type="text/css">
            section.card {
                display: flex;
                flex-direction: column;
                color: rgba(0, 0, 0, 0.75);
            }

            section.card > form {
                width: 100%;
                display: flex;
                flex-direction: column;
                flex-grow: 1;
            }

            section.card label > span {
                font-size: 0.75rem;
                color: rgba(0, 0, 0, 0.5);
            }

            section.card input, section.card textarea, section.card select {
                margin: 0 0 0.5rem 0;
            }

            section.card textarea {
                resize: none;
                height: 5rem;
            }

            section.card > form > button {
                align-self: flex-end;
            }

            #ingredient-choice {
                display: flex;
                align-items: stretch;
            }

            #ingredient-choice > div {
                display: flex;
                flex-direction: column;
                margin-right: 1rem;
            }

            #ingredient-choice > #ingredient-choice-nutrient {
                flex-grow: 1;
            }

            #ingredient-choice > #ingredient-choice-weight {
                width: 5rem;
                flex-shrink: 0;
            }

            #ingredient-list {
                height: 18rem;
                overflow-y: scroll;
                border: 0.0625rem solid #d8d8d8;
                border-radius: 0.1875rem;
                padding: 0.25rem;
                background-color: #ffffff;
            }

            #ingredient-choice > #ingredient-choice-button {
                justify-content: flex-end;
                margin: 0;
                flex-shrink: 0;
            }

            #add-ingredient-button {
                margin: 0 0 0.5rem 0;
                align-items: flex-end;
            }

            .ingredient-row {
                display: flex;
                align-items: center;
                justify-content: space-between;
                margin-bottom: 0.5rem;
            }

            .ingredient-row .ingredient-row-left {
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .ingredient-row .ingredient-row-right {
                display: flex;
                align-items: inherit;
                flex-shrink: 0;
            }

            .ingredient-row .ingredient-row-right > * {
                margin-left: 0.5rem;
                flex-shrink: 0;
            }

            div.radio-option {
                display: flex;
                align-items: center;
                margin-bottom: 0.25rem;
            }

            div.radio-option > input[type = radio] {
                margin-bottom: 0;
            }

            div.radio-option > input[type = radio]:checked {
                margin-bottom: 0;
            }

            div.radio-option > label {
                margin-left: 0.25rem;
                flex-grow: 1;
            }
        </style>

        <script type="application/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.js"></script>

        <script type="application/javascript">
            var nutrition = angular.module("nutrition", []);

            nutrition.controller('NutrientController', ['$scope', '$http', function($scope, $http) {
                $scope.addIngredient = function(ingredient) {
                    if(!ingredient.nutrient || !ingredient.weight) {
                        return;
                    }

                    $scope.form.ingredients.push({
                        id: ingredient.nutrient.id,
                        name: ingredient.nutrient.name,
                        weight: ingredient.weight
                    });
                    $scope.currentIngredient = {};
                };

                $scope.removeIngredient = function(index) {
                    $scope.form.ingredients.splice(index, 1);
                };

                $scope.createNutrient = function() {
                    var data = angular.copy($scope.form.generic);
                    var isFormValid = false;
                    if(data.type == 'SimpleNutrient') {
                        data.components = $scope.form.components;
                        isFormValid = $scope.createSimpleNutrientForm.$valid;
                    } else {
                        data.ingredients = $scope.form.ingredients;
                        isFormValid = $scope.createCompositeNutrientForm.$valid && $scope.form.ingredients.length > 0;
                    }

                    if(!isFormValid) {
                        /* TODO: figure out how to do proper validation in Angular */
                        alert("Please fill in the form correctly. (At least one ingredient must be selected)");
                    }

                    if($scope.mode == 'create') {
                        $http.post("/nutrient/create", data).success(function(data) {
                            window.open('/nutrient/' + data.id, '_self');
                        });
                    } else {
                        alert("Not yet implemented: update nutrient " + $scope.nutrientId);
                    }
                };

                $scope.initializeCompositeNutrient = function(nutrient) {
                    $scope.currentIngredient = {};
                    $http.get("/nutrient/" + nutrient.id + "/ingredients").success(function(ingredients) {
                        angular.forEach(ingredients, function(ingredient) {
                            $scope.addIngredient(ingredient);
                        })
                    });
                };

                $scope.initializeSimpleNutrient = function(nutrient) {
                    $scope.form.components.energy = nutrient.components['energy'] / 10;
                    $scope.form.components.proteins = nutrient.components['proteins'] * 100;
                    $scope.form.components.carbs = nutrient.components['carbs'] * 100;
                    $scope.form.components.fats = nutrient.components['fats'] * 100;
                    $scope.form.components.fruits = nutrient.components['fruits'] * 100;
                    $scope.form.components.vegetables = nutrient.components['vegetables'] * 100;
                };

                $scope.initializeEdit = function() {
                    $http.post('/nutrient/' + $scope.nutrientId).success(function(nutrient) {
                        $scope.form.generic.type = nutrient.type;
                        $scope.form.generic.name = nutrient.name;
                        $scope.form.generic.comments = nutrient.comments;
                        if(nutrient.type == 'SimpleNutrient') {
                            $scope.initializeSimpleNutrient(nutrient);
                        } else if(nutrient.type == 'CompositeNutrient') {
                            $scope.initializeCompositeNutrient(nutrient);
                        } else {
                            alert("ERROR: Unknown nutrient type");
                        }
                    });
                };

                $scope.initialize = function() {
                    $scope.form = {
                        generic: {
                            type: 'SimpleNutrient'
                        },
                        components: {},
                        ingredients: []
                    };

                    var pathSegments = location.toString().split("/");
                    $scope.mode = pathSegments.pop();
                    if($scope.mode == 'edit') {
                        $scope.nutrientId = pathSegments.pop();
                        $scope.initializeEdit();
                    } else if($scope.mode != 'create') {
                        alert("ERROR: Unknown mode.");
                    }

                    $http.get("/nutrients/get").success(function(data) {
                        $scope.availableNutrients = data;
                    });
                };

                $scope.initialize();
            }]);
        </script>
    </head>
    <body ng-app="nutrition" ng-controller="NutrientController">
        <header>
            <h1>{{mode}} nutrient</h1>
            <p>This page allows you to {{mode}} a nutrient.</p>
        </header>
        <main>
            <section class="card medium interactable" ng-show="mode == 'create'">
                <header>Select nutrient type</header>
                <div class="radio-option">
                    <input type="radio" name="nutrientType" value="SimpleNutrient" ng-model="form.generic.type"/>
                    <label>Simple nutrient</label>
                </div>
                <div class="radio-option">
                    <input type="radio" name="nutrientType" value="CompositeNutrient" ng-model="form.generic.type"/>
                    <label>Composite nutrient</label>
                </div>
            </section>
            <section class="card medium interactable" ng-show="form.generic.type == 'SimpleNutrient'">
                <header>{{mode}} Simple Nutrient</header>
                <form name="createSimpleNutrientForm">
                    <label>Name:</label>
                    <input type="text" required ng-model="form.generic.name"/>

                    <br/>

                    <label>Energy: <span>(kJ per 100g)</span></label>
                    <input type="number" required min="0" max="{{37 * 100}}" ng-model="form.components.energy"/>

                    <label>Proteins: <span>(g per 100g)</span></label>
                    <input type="number" required step="any" min="0" max="100" ng-model="form.components.proteins"/>

                    <label>Carbs: <span>(g per 100g)</span></label>
                    <input type="number" required step="any" min="0" max="100" ng-model="form.components.carbs"/>

                    <label>Fats: <span>(g per 100g)</span></label>
                    <input type="number" required step="any" min="0" max="100" ng-model="form.components.fats"/>

                    <label>Fiber: <span>(g per 100g)</span></label>
                    <%-- TODO: set to required --%>
                    <input type="number" step="any" min="0" max="100" ng-model="form.components.fiber" disabled/>

                    <br/>

                    <label>Fruits: <span>(g per 100g)</span></label>
                    <input type="number" requiredstep="any" min="0" max="100" ng-model="form.components.fruits"/>

                    <label>Vegetables: <span>(g per 100g)</span></label>
                    <input type="number" required step="any" min="0" max="100" ng-model="form.components.vegetables"/>

                    <br/>

                    <label>Comments:</label>
                    <textarea ng-model="form.generic.comments"></textarea>

                    <button ng-click="createNutrient()">{{mode}} simple nutrient</button>
                </form>
            </section>
            <section class="card medium interactable" ng-show="form.generic.type == 'CompositeNutrient'">
                <header>{{mode}} Composite Nutrient</header>
                <form name="createCompositeNutrientForm">
                    <label>Name:</label>
                    <input type="text" required ng-model="form.generic.name"/>

                    <br/>

                    <div id="ingredient-choice">
                        <div id="ingredient-choice-nutrient">
                            <label>Nutrient:</label>
                            <select ng-model="currentIngredient.nutrient" ng-options="nutrient.name for nutrient in availableNutrients"></select>
                        </div>
                        <div id="ingredient-choice-weight">
                            <label>Weight: <span>(g)</span></label>
                            <input type="number" min="0" ng-model="currentIngredient.weight"/>
                        </div>
                        <div id="ingredient-choice-button">
                            <button id="add-ingredient-button" ng-click="addIngredient(currentIngredient)">Add</button>
                        </div>
                    </div>

                    <label>Selected Ingredients:</label>
                    <div id="ingredient-list">
                        <div class="ingredient-row" ng-repeat="ingredient in form.ingredients">
                            <div class="ingredient-row-left">{{ingredient.name}}</div>
                            <div class="ingredient-row-right">
                                <span>{{ingredient.weight}}g</span>
                                <button ng-click="removeIngredient($index)">Remove</button>
                            </div>
                        </div>
                    </div>

                    <br/>

                    <label>Comments:</label>
                    <textarea ng-model="form.generic.comments"></textarea>

                    <button ng-click="createNutrient()">{{mode}} composite nutrient</button>
                </form>
            </section>
            <%--<section class="card large">--%>
                <%--<header>Large</header>--%>
            <%--</section>--%>
            <%--<section class="card medium">--%>
                <%--<header>Medium</header>--%>
            <%--</section>--%>
            <%--<section class="card medium">--%>
                <%--<header>Medium</header>--%>
            <%--</section>--%>
            <%--<section class="card small">--%>
                <%--<header>Small</header>--%>
            <%--</section>--%>
            <%--<section class="card small">--%>
                <%--<header>Small</header>--%>
            <%--</section>--%>
            <%--<section class="card small">--%>
                <%--<header>Small</header>--%>
            <%--</section>--%>
            <%--<section class="card tiny">--%>
                <%--<header>Tiny</header>--%>
            <%--</section>--%>
            <%--<section class="card tiny">--%>
                <%--<header>Tiny</header>--%>
            <%--</section>--%>
            <%--<section class="card tiny">--%>
                <%--<header>Tiny</header>--%>
            <%--</section>--%>
            <%--<section class="card tiny">--%>
                <%--<header>Tiny</header>--%>
            <%--</section>--%>
        </main>
    </body>
</html>
