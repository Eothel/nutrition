package be.rivendale.nutrition;

import be.rivendale.nutrition.day.Day;
import be.rivendale.nutrition.day.DayController;
import be.rivendale.nutrition.domain.Ingredient;
import be.rivendale.nutrition.nutrient.Nutrient;
import be.rivendale.nutrition.persistence.PersistenceTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Date;

import static java.time.Month.JULY;
import static java.time.Month.MARCH;
import static org.junit.Assert.assertNull;

public class DayControllerTest extends PersistenceTest {
    @Autowired
    private DayController controller;

    private Nutrient nutrient;

    private Date unexistingDate() {
        return newDate(2014, MARCH, 1);
    }

    private Date existingDate() {
        return newDate(2014, MARCH, 30);
    }

    @Before
    public void setupExistingDay() {
        Day day = new Day(existingDate());
        nutrient = nutrientByName("Vers - Sinaasappel");
        day.addEntry(new Ingredient(nutrient, 250));
        persistFlushClearAndRefetch(day);
    }

    @Before
    public void simulateNow() {
        DateUtilities.simulatedDate = LocalDate.of(1983, JULY, 15);
    }

    @After
    public void desimulateNow() {
        DateUtilities.simulatedDate = null;
    }

    @Test
    public void testTodayDelegatesToDayWithCurrentDay() throws Exception {
        assertEquals("redirect:1983-07-15", controller.today());
    }

    @Test
    public void testAddingAnEntryToANonExistingDayAddsANewDayWithSaidEntry() throws Exception {
        Day day = controller.add(unexistingDate(), nutrient.getId(), 250);
        entityManager().flush();

        assertNotNull(day.getId());
        assertDoubleEquals(250, day.getEntries().get(0).getWeight());
        assertEquals("Vers - Sinaasappel", day.getEntries().get(0).getNutrient().getName());
    }

    @Test
    public void testAddingAnEntryToAnExistingDayAddsSaidEntryToExistingDay() throws Exception {
        Day day = controller.add(existingDate(), nutrient.getId(), 125);
        assertEquals(2, day.getEntries().size());
    }

    @Test
    public void testDayJsonReturnsExistingDayIfPresent() throws Exception {
        Day day = controller.dayJson(existingDate());
        assertNotNull(day.getId());
    }

    @Test
    public void testDayJsonReturnsTransientDateOfNotPresent() throws Exception {
        Day day = controller.dayJson(unexistingDate());
        assertNull(day.getId());
    }
}
