package be.rivendale.nutrition;

import be.rivendale.nutrition.nutrient.SimpleNutrient;

public class DomainFactory {
    public static final String DUMMY_NUTRIENT_NAME = "dummy-nutrient-name";

    public static SimpleNutrient dummyNutrient() {
        return new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1, 2, 3, 4, 5, 6);
    }

    public static SimpleNutrient proteinSource(double proteins) {
        return new SimpleNutrient(DUMMY_NUTRIENT_NAME, proteins, 2, 3, 4, 5, 6);
    }
}
