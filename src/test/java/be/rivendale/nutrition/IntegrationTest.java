package be.rivendale.nutrition;

import be.rivendale.nutrition.configuration.ApplicationConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * An integration test is a test that requires the Spring context.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@ActiveProfiles("test")
public abstract class IntegrationTest extends BaseTest {
}
