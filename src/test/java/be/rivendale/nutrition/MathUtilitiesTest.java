package be.rivendale.nutrition;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class MathUtilitiesTest {
    @Test
    public void testEqualsReturnsTrueIfBothDoublesAreEqual() throws Exception {
        assertTrue(MathUtilities.doubleEquals(1.234, 1.234));
    }

    @Test
    public void testEqualsReturnsTrueIfDifferenceBetweenTwoDoublesIsSmallerThenOneThousandth() throws Exception {
        assertTrue(MathUtilities.doubleEquals(1.1236, 1.1235));
    }

    @Test
    public void testEqualsReturnsFalseIfBothDoublesAreNotEqual() throws Exception {
        assertFalse(MathUtilities.doubleEquals(1.123, 4.567));
    }
}
