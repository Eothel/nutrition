package be.rivendale.nutrition.domain;

import be.rivendale.nutrition.nutrient.CompositeNutrient;
import be.rivendale.nutrition.nutrient.NutrientComponent;
import be.rivendale.nutrition.nutrient.SimpleNutrient;
import be.rivendale.nutrition.persistence.PersistenceTest;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;

import static be.rivendale.nutrition.nutrient.NutrientComponent.*;

public class NutrientPersistenceTest extends PersistenceTest {

    public static final String DUMMY_NUTRIENT_NAME = "dummy-nutrient-name";

    @Test
    public void testCompoundNutrientCanBePersisted() throws Exception {
        CompositeNutrient nutrient = persist(new CompositeNutrient(DUMMY_NUTRIENT_NAME, null, ingredients()));
        assertNotNull(nutrient.getId());
    }

    @Test
    public void testSimpleNutrientStoresComponentsCorrectly() throws Exception {
        SimpleNutrient nutrient = persistFlushClearAndRefetch(new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1.25, 1.50, 1.75, 2, 2.25, 2.50));
        Map<NutrientComponent, Double> components = nutrient.getComponents();
        assertDoubleEquals(2, components.get(energy));
        assertDoubleEquals(1.75, components.get(fats));
        assertDoubleEquals(1.25, components.get(proteins));
        assertDoubleEquals(1.5, components.get(carbs));
        assertDoubleEquals(2.25, components.get(fruits));
        assertDoubleEquals(2.50, components.get(vegetables));
    }

    @Test
    public void testCompoundNutrientsArePersisted() throws Exception {
        CompositeNutrient nutrient = persistFlushClearAndRefetch(new CompositeNutrient(DUMMY_NUTRIENT_NAME, null, ingredients()));
        assertEquals(3, nutrient.ingredients().size());
    }

    private Ingredient[] ingredients() {
        double ratio = 1.0 / 3;
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(new Ingredient(new SimpleNutrient("nutrient-a", 1, 2, 3, 4, 5, 6), ratio));
        ingredients.add(new Ingredient(new SimpleNutrient("nutrient-b", 5, 6, 7, 8, 9, 10), ratio));
        ingredients.add(new Ingredient(new SimpleNutrient("nutrient-c", 9, 10, 11, 12, 13, 14), ratio));
        return ingredients.toArray(new Ingredient[ingredients.size()]);
    }

    @Test
    public void testCommentsArePersisted() throws Exception {
        SimpleNutrient nutrient = persistFlushClearAndRefetch(new SimpleNutrient(DUMMY_NUTRIENT_NAME, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, "My comments under test", null));
        assertEquals("My comments under test", nutrient.getComments());
    }
}
