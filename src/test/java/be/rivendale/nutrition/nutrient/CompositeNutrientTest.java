package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.BaseTest;
import be.rivendale.nutrition.domain.Ingredient;
import be.rivendale.nutrition.domain.NutrientException;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.junit.Test;

import static be.rivendale.nutrition.nutrient.NutrientComponent.*;

public class CompositeNutrientTest extends BaseTest {
    private static final String DUMMY_NUTRIENT_NAME = "My compound nutrient";

    private static final SimpleNutrient DUMMY_NUTRIENT_A = new SimpleNutrient("dummy-nutrient-a", 10, 5, 2, 100, 0, 1);
    private static final SimpleNutrient DUMMY_NUTRIENT_B = new SimpleNutrient("dummy-nutrient-b", 15.5, 14.1, 17.5, 12.1, 1, 0);
    private static final SimpleNutrient DUMMY_NUTRIENT_C = new SimpleNutrient("dummy-nutrient-c", 8, 5, 7, 9, 0, 0.5);
    private static final SimpleNutrient DUMMY_NUTRIENT_D = new SimpleNutrient("dummy-nutrient-d", 54.57, 14.12, 25.89, 14.74, 0.25, 0);

    @Test(expected = NutrientException.class)
    public void testNewCompoundNutrientWithEqualIngredientsMustNotPassNull() throws Exception {
        new CompositeNutrient(DUMMY_NUTRIENT_NAME, null, (SimpleNutrient[])null);
    }

    @Test(expected = NutrientException.class)
    public void testNewCompoundNutrientWithEqualIngredientsMustNotPassEmptyArray() throws Exception {
        new CompositeNutrient(DUMMY_NUTRIENT_NAME, null, new Nutrient[0]);
    }

    @Test
    public void testNewCompoundNutrientWithoutIngredientsCreateEquallyNormalizedIngredients() throws Exception {
        CompositeNutrient compositeNutrient = new CompositeNutrient("My compound nutrient", null,
                DUMMY_NUTRIENT_A,
            DUMMY_NUTRIENT_B,
            DUMMY_NUTRIENT_C,
            DUMMY_NUTRIENT_D
        );
        double expectedWeight = 1.0 / compositeNutrient.ingredients().size();
        for (Ingredient ingredient : compositeNutrient.ingredients()) {
            assertDoubleEquals(expectedWeight, ingredient.getWeight());
        }
    }

    @Test(expected = NutrientException.class)
    public void testNewCompoundNutrientWithIngredientsMustNotPassNull() throws Exception {
        new CompositeNutrient(DUMMY_NUTRIENT_NAME, null, (Ingredient[])null);
    }

    @Test(expected = NutrientException.class)
    public void testNewCompoundNutrientWithIngredientsMustNotPassEmptyArray() throws Exception {
        new CompositeNutrient(DUMMY_NUTRIENT_NAME, null, new Ingredient[0]);
    }

    @Test
    public void testNewCompoundNutrientNormalizesIngredients() throws Exception {
        CompositeNutrient nutrient = new CompositeNutrient(DUMMY_NUTRIENT_NAME, null,
                new Ingredient(DUMMY_NUTRIENT_A, 100),
            new Ingredient(DUMMY_NUTRIENT_B, 200),
            new Ingredient(DUMMY_NUTRIENT_C, 300),
            new Ingredient(DUMMY_NUTRIENT_D, 400)
        );
        assertDoubleEquals(0.1, findNutrientByName(nutrient, DUMMY_NUTRIENT_A.getName()).getWeight());
        assertDoubleEquals(0.2, findNutrientByName(nutrient, DUMMY_NUTRIENT_B.getName()).getWeight());
        assertDoubleEquals(0.3, findNutrientByName(nutrient, DUMMY_NUTRIENT_C.getName()).getWeight());
        assertDoubleEquals(0.4, findNutrientByName(nutrient, DUMMY_NUTRIENT_D.getName()).getWeight());

    }

    @Test
    public void testProteinsIsWeightedAverageOfCompoundsMembers() throws Exception {
        assertDoubleEquals(28.328, dummyCompoundNutrient().getComponents().get(proteins));
    }

    @Test
    public void testCarbsIsWeightedAverageOfCompoundsMembers() throws Exception {
        assertDoubleEquals(10.468, dummyCompoundNutrient().getComponents().get(carbs));
    }

    @Test
    public void testFatsIsWeightedAverageOfCompoundsMembers() throws Exception {
        assertDoubleEquals(16.156, dummyCompoundNutrient().getComponents().get(fats));
    }

    @Test
    public void testEnergyIsWeightedAverageOfCompoundsMembers() throws Exception {
        assertDoubleEquals(21.016, dummyCompoundNutrient().getComponents().get(energy));
        assertDoubleEquals(21.016, dummyCompoundNutrient().getEnergy());
    }

    @Test
    public void testFruitsIsWeightedAverageOfCompoundMembers() throws Exception {
        assertDoubleEquals(0.3, dummyCompoundNutrient().getComponents().get(fruits));
    }

    @Test
    public void testVegetablesIsWeightedAverageOfCompoundMembers() throws Exception {
        assertDoubleEquals(0.25, dummyCompoundNutrient().getComponents().get(vegetables));
    }

    @Test
    public void testNutrientNameIsMandatory() throws Exception {
        expectedException(NutrientException.class, "Nutrient name is mandatory");
        new CompositeNutrient(null, null, DUMMY_NUTRIENT_A, DUMMY_NUTRIENT_B);
    }

    @Test
    public void testNutrientNameMustNotBeBlank() throws Exception {
        expectedException(NutrientException.class, "Nutrient name is mandatory");
        new CompositeNutrient(" ", null, DUMMY_NUTRIENT_A, DUMMY_NUTRIENT_B);
    }

    @Test
    public void testNutrientNameMustNotBeEmpty() throws Exception {
        expectedException(NutrientException.class, "Nutrient name is mandatory");
        new CompositeNutrient("", null, DUMMY_NUTRIENT_A, DUMMY_NUTRIENT_B);
    }

    @Test
    public void testCommentsAreSet() throws Exception {
        CompositeNutrient nutrient = new CompositeNutrient("Nutrient with comments", "Some cool comment", DUMMY_NUTRIENT_A, DUMMY_NUTRIENT_B);
        assertEquals("Some cool comment", nutrient.getComments());
    }

    private CompositeNutrient dummyCompoundNutrient() {
        return new CompositeNutrient(DUMMY_NUTRIENT_NAME, null,
                new Ingredient(DUMMY_NUTRIENT_A, 10),
            new Ingredient(DUMMY_NUTRIENT_B, 20),
            new Ingredient(DUMMY_NUTRIENT_C, 30),
            new Ingredient(DUMMY_NUTRIENT_D, 40)
        );
    }

    private Ingredient findNutrientByName(CompositeNutrient nutrient, final String nutrientName) {
        return Iterables.find(nutrient.ingredients(), new Predicate<Ingredient>() {
            @Override
            public boolean apply(Ingredient input) {
                return input.getNutrient().getName().equals(nutrientName);
            }
        });
    }
}
