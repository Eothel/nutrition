package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.domain.Ingredient;
import be.rivendale.nutrition.domain.NutrientException;
import be.rivendale.nutrition.persistence.PersistenceTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static be.rivendale.nutrition.nutrient.NutrientComponent.*;

public class NutrientControllerTest extends PersistenceTest {
    @Autowired
    private NutrientController controller;

    @Test
    public void testGetWithEmptyFilterReturnsAllNutrients() throws Exception {
        List<Nutrient> nutrients = controller.get("");
        assertEquals(111, nutrients.size());
    }

    @Test
    public void testGetWithFilterReturnsSubsetOfNutrients() throws Exception {
        List<Nutrient> nutrients = controller.get("Ringis");
        assertEquals(5, nutrients.size());
    }

    @Test
    public void testGetFilterIsCaseInsensitive() throws Exception {
        Nutrient nutrient = controller.get("BaNaAn").get(0);
        assertEquals("Vers - Banaan", nutrient.getName());
    }

    @Test
    public void testGetReturnsNutrientsSortedByName() throws Exception {
        List<Nutrient> nutrients = controller.get("melk");
        assertTrue(nutrients.get(0).getName().startsWith("D"));
        assertTrue(nutrients.get(1).getName().startsWith("E"));
        assertTrue(nutrients.get(2).getName().startsWith("K"));
        assertTrue(nutrients.get(3).getName().startsWith("L"));
    }

    public static final String DUMMY_NUTRIENT_NAME = "Dummy nutrient name";

    private CreateNutrientForm createFormForSimpleNutrient(String name, double proteins, double carbs, double fats, double energy, double fruits, double vegetables, String comments) {
        CreateNutrientForm form = new CreateNutrientForm();
        form.name = name;
        form.type = "SimpleNutrient";
        form.comments = comments;
        form.components.proteins = proteins;
        form.components.carbs = carbs;
        form.components.fats = fats;
        form.components.energy = energy;
        form.components.fruits = fruits;
        form.components.vegetables = vegetables;
        return form;
    }

    private CreateNutrientForm.IngredientForm createIngredientForm(int nutrientId, double weight) {
        CreateNutrientForm.IngredientForm ingredient = new CreateNutrientForm.IngredientForm();
        ingredient.id = nutrientId;
        ingredient.weight = weight;
        return ingredient;
    }

    @Test
    public void testCreateNutrientReturnsSimpleNutrientIfTheCorrespondingFieldsAreFilledIn() throws Exception {
        CreateNutrientForm form = createFormForSimpleNutrient(DUMMY_NUTRIENT_NAME, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, null);
        SimpleNutrient nutrient = (SimpleNutrient) controller.createNutrient(form);
        assertEquals(DUMMY_NUTRIENT_NAME, nutrient.getName());
        assertDoubleEquals(0.01, nutrient.getComponents().get(proteins));
        assertDoubleEquals(0.02, nutrient.getComponents().get(carbs));
        assertDoubleEquals(0.03, nutrient.getComponents().get(fats));
        assertDoubleEquals(40, nutrient.getComponents().get(energy));
        assertDoubleEquals(0.05, nutrient.getComponents().get(fruits));
        assertDoubleEquals(0.06, nutrient.getComponents().get(vegetables));
    }

    @Test
    public void testCreateNutrientReturnsCompositeNutrientIfTheCorrespondingFieldsAreFilledIn() throws Exception {
        CreateNutrientForm form = createFormForCompositeNutrient();
        CompositeNutrient nutrient = (CompositeNutrient) controller.createNutrient(form);
        assertEquals(DUMMY_NUTRIENT_NAME, nutrient.getName());

        ArrayList<Ingredient> ingredients = new ArrayList<>(nutrient.ingredients());
        ingredients.sort((a, b) -> a.getNutrient().getName().compareTo(b.getNutrient().getName()));
        assertEquals(2, ingredients.size());
        assertIngredient("Everyday - Magere Melk", 0.961538, ingredients.get(0));
        assertIngredient("Nestle - Nesquik", 0.038462, ingredients.get(1));
    }

    @Test
    public void testCreateNutrientPopulatesCommentForCompositeNutrient() throws Exception {
        CreateNutrientForm form = createFormForCompositeNutrient();
        Nutrient nutrient = controller.createNutrient(form);
        assertEquals("Composite comment", nutrient.getComments());
    }

    private CreateNutrientForm createFormForCompositeNutrient() {
        CreateNutrientForm form = new CreateNutrientForm();
        form.name = DUMMY_NUTRIENT_NAME;
        form.type = "CompositeNutrient";
        form.comments = "Composite comment";
        form.ingredients.add(createIngredientForm(nutrientByName("Everyday - Magere Melk").getId(), 250.0));
        form.ingredients.add(createIngredientForm(nutrientByName("Nestle - Nesquik").getId(), 10.0));
        return form;
    }

    private void assertIngredient(String expectedNutrientName, double expectedIngredientWeight, Ingredient ingredient) {
        assertEquals(expectedNutrientName, ingredient.getNutrient().getName());
        assertDoubleEquals(expectedIngredientWeight, ingredient.getWeight());
    }

    @Test
    public void testAsNutrientRequiresName() throws Exception {
        expectedException(NutrientException.class, "Nutrient name is mandatory");
        CreateNutrientForm form = createFormForSimpleNutrient(null, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, null);
        controller.createNutrient(form);
    }

    @Test
    public void testCreateNutrientPersistsNewNutrient() throws Exception {
        Nutrient nutrient = controller.createNutrient(createFormForSimpleNutrient(DUMMY_NUTRIENT_NAME, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, null));
        assertNotNull(nutrient.getId());
    }

    @Test
    public void testCreateNutrientPopulatesCommentForSimpleNutrient() throws Exception {
        Nutrient nutrient = controller.createNutrient(createFormForSimpleNutrient(DUMMY_NUTRIENT_NAME, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, "My comment"));
        assertEquals("My comment", nutrient.getComments());
    }

    @Test
    public void testIngredientsByNutrientReturnsAllIngredientsOfACompositeNutrientOrderedByNutrientName() throws Exception {
        List<Ingredient> ingredients = controller.ingredientsByNutrient(nutrientByName("Mama - Pompoensoep").getId());
        assertEquals("Boursin - Cuisine: Tomaat & Mediterrane Kruiden", ingredients.get(0).getNutrient().getName());
        assertEquals("Generic - Zout", ingredients.get(1).getNutrient().getName());
        assertEquals("Generic - Zwarte Peper", ingredients.get(2).getNutrient().getName());
        assertEquals("Knorr - Bouillon Keteltje: Kip", ingredients.get(3).getNutrient().getName());
        assertEquals("Ringis - Pompoen In Kubusblokjes (diepvries)", ingredients.get(4).getNutrient().getName());
        assertEquals("Ringis - Wortelschijfjes (diepvries)", ingredients.get(5).getNutrient().getName());
        assertEquals("Vers - Aardappelen", ingredients.get(6).getNutrient().getName());
        assertEquals("Vers - Paprika: Rood", ingredients.get(7).getNutrient().getName());
        assertEquals("Vers - Uien", ingredients.get(8).getNutrient().getName());
        assertEquals("Water", ingredients.get(9).getNutrient().getName());
    }

    @Test
    public void testIngredientsByNutrientReturnsEmptySetInCaseOfSimpleNutrient() throws Exception {
        List<Ingredient> ingredients = controller.ingredientsByNutrient(nutrientByName("Lays - SuperChips Salt ‘N Pepper").getId());
        assertEquals(0, ingredients.size());
    }
}
