package be.rivendale.nutrition.nutrient;

import be.rivendale.nutrition.BaseTest;
import org.junit.Test;

public class ServingTest extends BaseTest {
    @Test
    public void testWeightReturnsAverageMeasurement() throws Exception {
        Serving serving = new Serving("myServing", 15, 7);
        assertDoubleEquals(11, serving.getWeight());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMeasurementsMustNotBeNull() throws Exception {
        new Serving("invalid serving", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMeasurementsMustNotBeEmpty() throws Exception {
        new Serving("invalid serving", new double[0]);
    }
}
