package be.rivendale.nutrition.persistence;

import be.rivendale.nutrition.IntegrationTest;
import be.rivendale.nutrition.domain.Identifiable;
import be.rivendale.nutrition.nutrient.Nutrient;
import org.junit.Before;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

@Transactional
@TransactionConfiguration
public abstract class PersistenceTest extends IntegrationTest {
    @PersistenceContext
    private EntityManager entityManager;

    @Before
    public void loadTestData() throws Exception {
        new TestDataLoader(entityManager).load();
    }

    protected EntityManager entityManager() {
        return this.entityManager;
    }

    protected  <T> T persist(T entity) {
        return PersistenceTestUtilities.persist(entityManager, entity);
    }

    protected <T> T find(Class<? extends T> clazz, Serializable id) {
        return PersistenceTestUtilities.find(entityManager, clazz, id);
    }

    protected <T> T query(String query, Class<T> entityClass) {
        return PersistenceTestUtilities.query(entityManager, query, entityClass);
    }

    protected Nutrient nutrientByName(String nutrientName) {
        return PersistenceTestUtilities.nutrientByName(entityManager, nutrientName);
    }

    protected <T extends Identifiable> T persistFlushClearAndRefetch(T entity) {
        return PersistenceTestUtilities.persistFlushClearAndRefetch(entityManager, entity);
    }
}
