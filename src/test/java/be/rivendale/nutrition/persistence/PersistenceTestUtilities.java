package be.rivendale.nutrition.persistence;

import be.rivendale.nutrition.domain.Identifiable;
import be.rivendale.nutrition.nutrient.Nutrient;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public class PersistenceTestUtilities {
     static <T> T persist(EntityManager entityManager, T entity) {
        entityManager.persist(entity);
        return entity;
    }

    static <T> T find(EntityManager entityManager, Class<? extends T> clazz, Serializable id) {
        return entityManager.find(clazz, id);
    }

    static <T> T query(EntityManager entityManager, String query, Class<T> entityClass) {
        List<T> result = entityManager.createQuery(query, entityClass).getResultList();
        if(result.isEmpty()) {
            return null;
        } else {
            return result.iterator().next();
        }
    }

    static Nutrient nutrientByName(EntityManager entityManager, String nutrientName) {
        Nutrient nutrient = query(entityManager, "select n from " + Nutrient.class.getSimpleName() + " n where n.name = '" + nutrientName + "'", Nutrient.class);
        if(nutrient == null) {
            throw new IllegalArgumentException(String.format("Unable to find nutrient with name '%s'", nutrientName));
        }
        return nutrient;
    }

    static <T extends Identifiable> T persistFlushClearAndRefetch(EntityManager entityManager, T entity) {
        persist(entityManager, entity);
        entityManager.flush();
        entityManager.clear();
        return (T)find(entityManager, entity.getClass(), entity.getId());
    }
}
