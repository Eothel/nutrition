var assert = require('nodeunit').assert;

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

var schema = require('../../main/javascript/schema/schema');

module.exports = {
    setUp: function(callback) {
        console.log('Connecting Mongoose');
        mongoose.connect('mongodb://localhost/unit-test', function(error) {
            assert.ifError(error);
            mongoose.connection.db.dropCollection('nutrients', function() {
                assert.ifError(error);
                mongoose.connection.db.dropCollection('days', function() {
                    assert.ifError(error);
                    initializeDatabase(callback);
                });
            });
        });
    },

    tearDown: function(callback) {
        console.log('Disconnecting Mongoose');
        mongoose.disconnect(function(error) {
            assert.ifError(error);
            callback();
        });
    },

    'Resolving macronutrient component from unpopulated ingredient throws error': function(test) {
        test.expect(7);
        schema.Nutrient.findOne({name: 'Day entry one'}, function(error, day) {
            test.ifError(error);
            assertThrowsForComponent(test, day, 'energy');
            assertThrowsForComponent(test, day, 'proteins');
            assertThrowsForComponent(test, day, 'carbs');
            assertThrowsForComponent(test, day, 'fats');
            assertThrowsForComponent(test, day, 'fruits');
            assertThrowsForComponent(test, day, 'vegetables');
            test.done();
        });
    },

    'Day resolves macronutrient components through virtual property recursively': function(test) {
        test.expect(8);
        schema.Day.findOne().populate('entries.nutrient').exec(function(error, day) {
            test.ifError(error);
            schema.Nutrient.populate(day.entries, 'nutrient.ingredients.nutrient', function(error) {
                test.ifError(error);
                test.equal(day.energy, 419710680);
                test.floatEqual(day.proteins, 4051.56);
                test.floatEqual(day.carbs, 14435.58);
                test.floatEqual(day.fats, 4647.36);
                test.floatEqual(day.fruits, 15738.54);
                test.floatEqual(day.vegetables, 7688.7);
                test.done();
            });
        });
    },

    'Recursive populate on day populates all entries': function(test) {
        test.expect(1);
        schema.Day.findOne().exec(function(error, day) {
            day.recursivePopulate(function() {
                test.equal(day.entries[0].nutrient.ingredients[0].nutrient.name, 'One');
                test.done();
            });
        });
    },

    'Day.findByDate() returns fully populated day if exists and flag is high': function(test) {
        test.expect(2);
        schema.Day.findByDate(new Date('2014-06-24T00:00:00+0200'), true, function(error, day) {
            test.equal(error, null);
            test.equal(day.energy, 419710680);
            test.done();
        });
    },

    'Day.findByDate() does not recursively populate if flag is low': function(test) {
        test.expect(2);
        schema.Day.findByDate(new Date('2014-06-24T00:00:00+0200'), false, function(error, day) {
            test.equal(error, null);
            test.equal(day.entries[0].nutrient.name, undefined);
            test.done();
        });
    },

    'Day.findByDate() returns null if not found': function(test) {
        test.expect(2);
        schema.Day.findByDate(new Date('2014-01-01T00:00:00+0200'), false, function(error, day) {
            test.equal(error, null);
            test.equal(day, null);
            test.done();
        });
    },

    'Day.saveAndPopulate() returns populated day after saving': function(test) {
        test.expect(2);
        schema.Day.findOne(function(error, foundDay) {
            test.equal(foundDay.entries.length, 2);
            schema.Nutrient.findOne(function(error, nutrient) {
                foundDay.entries.push({weight: 150, nutrient: nutrient._id});
                foundDay.saveAndPopulate(function(error, savedDay) {
                    test.equal(savedDay.energy, 419936730);
                    test.done();
                });
            });
        });
    },

    'Day.toJSON() includes virtuals': function(test) {
        test.expect(1);
        schema.Day.findOne().populate('entries.nutrient').exec(function(error, day) {
            day.entries[0].nutrient.recursivePopulate(function() {
                test.ok(day.toJSON().energy !== undefined);
                test.done();
            });
        });
    },

    'CompositeNutrient resolves macronutrient components through virtual property recursively': function(test) {
        test.expect(7);
        schema.Nutrient.findOne({ name: 'Day entry one' }).populate('ingredients.nutrient').exec(function(error, nutrient) {
            test.ifError(error);
            test.equal(nutrient.energy, 3446839);
            test.floatEqual(nutrient.proteins, 33.71);
            test.floatEqual(nutrient.carbs, 119.99);
            test.floatEqual(nutrient.fats, 38.67);
            test.floatEqual(nutrient.fruits, 131.14);
            test.floatEqual(nutrient.vegetables, 64);
            test.done();
        });
    },

    'CompositeNutrient.toJSON() includes virtuals': function(test) {
        test.expect(1);
        schema.Nutrient.findOne({name: 'Day entry one'}, function(error, nutrient) {
            nutrient.recursivePopulate(function() {
                test.ok(nutrient.toJSON().energy !== undefined);
                test.done();
            });
        });
    },

    'CompositeNutrient ingredients do not add id nor _id': function(test) {
        test.expect(2);
        schema.Nutrient.findOne({name: 'Day entry one'}, function(error, nutrient) {
            test.equal(nutrient.ingredients[0]._id, undefined);

            nutrient.ingredients[0]._id = ObjectId();
            test.equal(nutrient.ingredients[0].id, undefined);

            test.done();
        });
    },

    'Recursive populate on a SimpleNutrient does not do anything but invoke callback': function(test) {
        test.expect(1);
        schema.Nutrient.findOne({ name: 'Day entry two' }, function(error, nutrient) {
            nutrient.recursivePopulate(function() {
                test.ok(true);
                test.done();
            });
        });
    },

    'Recursive populate on a CompositeNutrient populates its nutrients': function(test) {
        test.expect(2);
        schema.Nutrient.findOne({ name: 'Day entry one' }, function(error, nutrient) {
            test.equal(nutrient.ingredients[0].nutrient.constructor.name, 'ObjectID');
            nutrient.recursivePopulate(function() {
                test.equal(nutrient.ingredients[0].nutrient.name, 'One');
                test.done();
            });
        });
    }
};

function initializeDatabase(callback) {
    var nestedOne = new schema.SimpleNutrient({ name: 'One', energy: 1507, proteins: 0.125, carbs: 0.536, fats: 0.23, fruits: 0.02, vegetables: 0 });
    var nestedTwo = new schema.SimpleNutrient({ name: 'Two', energy: 12540, proteins: 0.055, carbs: 0.14, fats: 0.01, fruits: 0.5, vegetables: 0.25 });

    var dayEntryOne = new schema.CompositeNutrient({
        name: 'Day entry one',
        ingredients: [
            {weight: 157, nutrient: nestedOne},
            {weight: 256, nutrient: nestedTwo}
        ]
    });
    var dayEntryTwo = new schema.SimpleNutrient({ name: 'Day entry two', energy: 35000, proteins: 0.04, carbs: 0.21, fats: 0.04, fruits: 0.01, vegetables: 0.05 });

    var day = new schema.Day({
        date: new Date('2014-06-24T00:00:00+0200'),
        entries: [
            { weight: 120, nutrient: dayEntryOne },
            { weight: 174, nutrient: dayEntryTwo }
        ]
    });

    nestedOne.save(function (error) {
        assert.ifError(error);
        nestedTwo.save(function (error) {
            assert.ifError(error);
            dayEntryOne.save(function (error) {
                assert.ifError(error);
                dayEntryTwo.save(function (error) {
                    assert.ifError(error);
                    day.save(function (error) {
                        assert.ifError(error);
                        callback();
                    });
                });
            });
        });
    });
}

assert.floatEqual = function(actual, expected) {
    assert.equal(Math.round(100 * actual) / 100, expected);
};

assert.throws = function(block, expectedMessageRegExp) {
    try {
        block();
        assert.fail('Expected exception to be thrown');
    } catch(error) {
        if(!(error instanceof Error) || !expectedMessageRegExp.test(error.message)) {
            assert.fail('Expected block to throw instance of Error with message matching regular expression `' + expectedMessageRegExp +'` but was `' + error.message + '`');
        }
    }
};

function assertThrowsForComponent(test, day, componentName) {
    test.throws(function() {
        var value = day[componentName];
    }, new RegExp('^Unable to resolve nutrient component `' + componentName + '` for `\\w*`\\. Did you populate this reference\\?$'));
}