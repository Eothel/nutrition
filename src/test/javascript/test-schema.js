var assert = require('nodeunit').assert;
var mongoose = require('mongoose');

var schema = require('./../../main/javascript/schema/schema');

var ObjectId = mongoose.Types.ObjectId;

module.exports = {
    'Nutrient id is automatically initialized': function(test) {
        var nutrient = new schema.SimpleNutrient({name: 'Kipfilet'});
        test.ok(nutrient._id);
        test.done();
    },

    'Nutrient name is initialized': function(test) {
        var nutrient = new schema.SimpleNutrient({name: 'Banaan'});
        test.equal(nutrient.name, 'Banaan');
        test.done();
    },

    'Nutrient name is required': function(test) {
        test.expect(1);
        var nutrient = new schema.SimpleNutrient();
        nutrient.validate(function(error) {
            test.required(error, 'name');
            test.done();
        });
    },

    'Nutrient type is initialized': function(test) {
        var simpleNutrient = new schema.SimpleNutrient();
        test.equal(simpleNutrient.__t, 'simple');

        var compositeNutrient = new schema.CompositeNutrient();
        test.equal(compositeNutrient.__t, 'composite');
        test.done();
    },

    'Nutrient comments are initialized': function(test) {
        var nutrient = new schema.SimpleNutrient({name: 'Peer', type: 'simple', comments: 'Lekker!'});
        test.equal(nutrient.comments, 'Lekker!');
        test.done();
    },

    'Nutrient serving is initialized': function(test) {
        var nutrient = new schema.SimpleNutrient({
            name: 'Aardbei',
            servings: [{
                label: 'Stuk',
                measurements: [5, 6, 4, 3, 8, 7, 5, 4, 5]
            }]
        });
        test.equal(nutrient.servings.length, 1);
        test.equal(nutrient.servings[0].label, 'Stuk');
        test.equal(nutrient.servings[0].measurements.join(), '5,6,4,3,8,7,5,4,5');
        test.done();
    },

    'Nutrient serving label is required': function(test) {
        test.expect(1);
        var nutrient = new schema.SimpleNutrient({
            name: 'Chocomelk',
            servings: [{
                label: null,
                measurements: [10, 20, 30]
            }]
        });
        nutrient.validate(function(error) {
            test.required(error, 'servings.0.label');
            test.done();
        });
    },

    'Nutrient serving measurements are required': function(test) {
        test.expect(1);
        var nutrient = new schema.SimpleNutrient({
            name: 'Chocomelk',
            servings: [{
                label: 'Mok'
            }]
        });
        nutrient.validate(function(error) {
            test.invalid(error, 'servings.0.measurements', 'At least one entry is required for "measurements".');
            test.done();
        });
    },

    'Nutrient serving measurements must be numbers': function(test) {
        test.expect(1);
        var nutrient = new schema.SimpleNutrient({
            name: 'Chocomelk',
            servings: [{
                label: 'Mok',
                measurements: ['a', 'b', 'c']
            }]
        });
        nutrient.validate(function(error) {
            test.invalid(error, 'servings.0.measurements', 'Entries must be numbers for "measurements".');
            test.done();
        });
    },

    'SimpleNutrient energy is initialized, required and ranges from 0 to 37000': function(test) {
        validateComponent(test, 'energy', 620, -0.01, 37000.01);
    },

    'SimpleNutrient proteins is initialized, required and ranges from 0 to 1': function(test) {
        validateComponent(test, 'proteins', 0.15, -0.01, 1.01);
    },

    'SimpleNutrient carbs is initialized and, required ranges from 0 to 1': function(test) {
        validateComponent(test, 'carbs', 0.45, -0.01, 1.01);
    },

    'SimpleNutrient fats is initialized and, required ranges from 0 to 1': function(test) {
        validateComponent(test, 'fats', 0.20, -0.01, 1.01);
    },

    'SimpleNutrient fruits is initialized, required and ranges from 0 to 1': function(test) {
        validateComponent(test, 'fruits', 1, -0.01, 1.01);
    },

    'SimpleNutrient vegetables is initialized, required and ranges from 0 to 1': function(test) {
        validateComponent(test, 'vegetables', 1, -0.01, 101);
    },

    'CompositeNutrient ingredients are initialized': function(test) {
        var nutrient = new schema.CompositeNutrient({
            name: 'Chocomelk',
            ingredients: [{
                weight: 100,
                nutrient: ObjectId()
            }]
        });
        test.equals(nutrient.ingredients[0].weight, 100);
        test.ok(nutrient.ingredients[0].nutrient);
        test.done();
    },

    'CompositeNutrient ingredient weight is required': function(test) {
        test.expect(1);
        var nutrient = new schema.CompositeNutrient({
            name: 'Chocomelk',
            ingredients: [{
                nutrient: ObjectId
            }]
        });
        nutrient.validate(function(error) {
            test.required(error, 'ingredients.0.weight');
            test.done();
        });
    },

    'CompositeNutrient ingredient weight must be positive': function(test) {
        test.expect(1);
        var nutrient = new schema.CompositeNutrient({
            name: 'Chocomelk',
            ingredients: [{
                weight: -1,
                nutrient: ObjectId
            }]
        });
        nutrient.validate(function(error) {
            test.invalid(error, 'ingredients.0.weight', 'Value for "weight" must be positive.');
            test.done();
        });
    },

    'CompositeNutrient ingredient reference is required': function(test) {
        test.expect(1);
        var nutrient = new schema.CompositeNutrient({
            name: 'Chocomelk',
            ingredients: [{
                weight: 50,
                nutrient: ObjectId
            }]
        });
        nutrient.validate(function(error) {
            test.required(error, "ingredients.0.nutrient");
            test.done();
        });
    },

    'CompositeNutrient ingredients must be of at least size one': function(test) {
        test.expect(1);
        var nutrient = new schema.CompositeNutrient({
            name: 'Groentenmix',
            ingredients: []
        });
        nutrient.validate(function(error) {
            test.match(error, 'ingredients', 'At least one ingredient is required', 'Empty ingredient array does not fail validation');
            test.done();
        });
    },

    'Day id is automatically initialized': function(test) {
        var day = new schema.Day({
            date: new Date()
        });
        test.ok(day._id !== null);
        test.equal(day.id, undefined);
        test.done();
    },

    'Day date is initialized': function(test) {
        var date = new Date();
        var day = new schema.Day({
            date: date
        });
        test.equal(day.date, date);
        console.log(day.date);
        test.done();
    },

    'Day date is required': function(test) {
        test.expect(1);
        var day = new schema.Day({});
        day.validate(function(error) {
            test.required(error, 'date');
            test.done();
        });
    },

    'Day entries are initialized': function(test) {
        var day = new schema.Day({
            date: new Date(),
            entries: [{
                weight: 100,
                nutrient: ObjectId()
            }]
        });
        test.equal(day.entries.length, 1);
        test.done();
    },

    'Day entries must be of at least size one': function(test) {
        test.expect(1);
        var day = new schema.Day({
            date: new Date(),
            entries: []
        });
        day.validate(function(error) {
            test.match(error, 'entries', 'At least one entry is required');
            test.done();
        });
    },

    'Day entry weight is required': function(test) {
        test.expect(1);
        var day = new schema.Day({
            date: new Date(),
            entries: [{}]
        });
        day.validate(function(error) {
            test.required(error, 'entries.0.weight');
            test.done();
        });
    },

    'Day entry weight must be positive': function(test) {
        test.expect(1);
        var day = new schema.Day({
            date: new Date(),
            entries: [{
                weight: 0
            }]
        });
        day.validate(function(error) {
            test.invalid(error, 'entries.0.weight', 'Value for "weight" must be positive.');
            test.done();
        });
    },

    'Day entry nutrient reference is required': function(test) {
        test.expect(1);
        var day = new schema.Day({
            date: new Date(),
            entries: [{
                weight: 157
            }]
        });
        day.validate(function(error) {
            test.required(error, 'entries.0.nutrient');
            test.done();
        });
    },

    'Day.removeEntry() removes a single entry at the specified index position': function(test) {
        test.expect(1);
        var day = new schema.Day({
            date: new Date(),
            entries: [{
                weight: 50, nutrient: ObjectId()
            }]
        });
        day.removeEntry(0);
        test.equal(day.entries.length, 0);
        test.done();
    },

    'Day.addEntry() adds a single entry at the end': function(test) {
        var day = new schema.Day({date: new Date()});
        day.addEntry({
            weight: 150,
            nutrient: new ObjectId()
        });
        test.equals(day.entries.length, 1);
        test.done();
    },
};

assert.required = function(error, propertyName) {
    if(error === undefined) {
        throw new Error('No errors registered, but expected error for property `' + propertyName +'`');
    }
    var propertyErrors = error.errors[propertyName];
    if(propertyErrors == undefined) {
        throw new Error('No required validation error found for property `' + propertyName + '`');
    }
    assert.equal(propertyErrors.message, 'Path `' + propertyName.split('.').pop() + '` is required.');
};

assert.error = function(error, propertyName) {
    if(error === undefined) {
        throw new Error('Validation error was expected for property `' + propertyName + '`')
    }
};

assert.invalid = function(error, propertyName, expectedMessage) {
    assert.error(error, propertyName);
    assert.equal(error.errors[propertyName].message, expectedMessage);
};

assert.match = function(error, propertyName, expression, message) {
    assert.error(error, propertyName);
    var actualMessage = error.errors[propertyName].message;
    var regExp = new RegExp(expression);
    if(!regExp.test(actualMessage)) {
        throw new Error(message);
    }
};

assert.minimum = function(error, propertyName) {
    assert.match(error, propertyName,
            'Path `' + propertyName + '` (.*) is less than minimum allowed value (.*).',
            'Property "' + propertyName + '" is not lower bounded'
    );
};

assert.maximum = function(error, propertyName) {
    assert.match(error, propertyName,
            'Path `' + propertyName + '` (.*) is more than maximum allowed value (.*).',
            'Property "' + propertyName + '" is not upper bounded'
    );
};

assert.invalidEnum = function(error, propertyName) {
    assert.match(error, propertyName,
            '`.*` is not a valid enum value for path `' + propertyName + '`',
            'Property "' + propertyName + '" did not cause an invalid enum validation message.'
    );
};

function validateRange(test, object, propertyName, lowerOutOfRange, upperOutOfRange, callback) {
    object[propertyName] = lowerOutOfRange;
    object.validate(function(error) {
        test.minimum(error, propertyName);
        object[propertyName] = upperOutOfRange;
        object.validate(function(error) {
            test.maximum(error, propertyName);
            callback();
        });
    });
}

function validateComponent(test, propertyName, inRangeValue, toLowValue, toHighValue) {
    var raw = {name: 'Komkommer', type: 'simple'};
    var nutrient = new schema.SimpleNutrient(raw);
    nutrient.validate(function(error) {
        test.required(error, propertyName);
        raw[propertyName] = inRangeValue;
        nutrient = new schema.SimpleNutrient(raw);
        test.equal(nutrient[propertyName], inRangeValue);
        validateRange(test, nutrient, propertyName, toLowValue, toHighValue, function() {
            test.done();
        });
    });
}